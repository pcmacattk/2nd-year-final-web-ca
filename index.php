<!DOCTYPE html>
<html>
    <head>
        <title>Infinity Bits</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/css.css"> 
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script src="JS/jquery-1.11.2.js"></script>
        <script src="JS/ajax.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <?php
                require_once ('includes/PDOConnect.php');
                include('includes/header.php'); 
                include('includes/aside.php');
                include('includes/nav.php');
            ?>
            <section>
                <h1>
                    <div id="systemnameOutput"></div> 
                </h1>
                <div id="output"></div>
            </section>
            <div id="comment">
                <table>
                    <tr>
                        <td rowspan="2">AVATAR</td>
                        <td>Time Posted</td>
                    </tr>
                    <tr>
                        <td>Comment</td>
                    </tr>
                 </table>
            </div>
        </div>
        <?php
            include('includes/footer.php');
        ?>
        
    </body>
</html>
