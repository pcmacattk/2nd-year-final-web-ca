<?php

class User
{
    private $name;
    private $password;
    private $avatar;
    
    function __construct($name, $password, $avatar) 
    {
        $this->name = $name;
        $this->password = $password;
        $this->avatar = $avatar;
    }

    function getName() 
    {
        return $this->name;
    }

    function getPassword() 
    {
        return $this->password;
    }

    function getAvatar() 
    {
        return $this->avatar;
    }

    function setName($name) 
    {
        $this->name = $name;
    }

    function setPassword($password) 
    {
        $this->password = $password;
    }

    function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    
    public function __toString() 
    {
        return "$this->name, $this->password, $this->avatar";
    }  

}
