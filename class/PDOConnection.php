<?php

class PDOConnection 
{
   private $pdoConnection;
   private $databaseName;
   private $hostName;
   private $userName;
   private $userPassword;
   private $statement;
   
   function __construct($databaseName, $hostName, $userName, $userPassword) {
       $this->databaseName = $databaseName;
       $this->hostName = $hostName;
       $this->userName = $userName;
       $this->userPassword = $userPassword;
       
       $dsn = "mysql:dbname=$databaseName;host=$hostName";
       try
       {
        $this->pdoConnection = new PDO($dsn, $this->userName, $this->userPassword);
        $this->pdoConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }
       catch(PDOException $e)
       {
           echo "Connection creation error:" . $e->getMessage(). PHP_EOL;
           echo "DSN: " . $dsn . PHP_EOL;
       }
    }
  
   function setStatement($sql)
   {
       try
       {
            $this->statement = $this->pdoConnection->prepare($sql);
       }
       catch(PDOException $e)
       {
           echo "Set statement error:" . $e->getMessage(). PHP_EOL;
           echo "SQL: " . $sql . PHP_EOL;
       }
   }
   
   function execute($arrayData)
   {
       try
       {
           if(!empty($arrayData) && !is_null($this->statement))
           {
                return $this->statement->execute($arrayData);
           }
       }
       catch(PDOException $e)
       {
           echo "Execute statement error:" . $e->getMessage() . PHP_EOL;
           echo "Data: " . implode(",", $arrayData) . PHP_EOL;
       }
   }
   
   function query($className)
   {
       try
       {
           if(is_string($className) && !empty($className) && !is_null($this->statement))
           {
                $this->statement->execute($arrayData);
                return $this->statement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
           }
       }
       catch(PDOException $e)
       {
           echo "Execute statement error:" . $e->getMessage() . PHP_EOL;
           echo "Data: " . implode(",", $arrayData) . PHP_EOL;
       }
   }
   
   function close()
   {
       try
       {
            unset($this->pdoConnection);
            unset($this->statement);
       }
       catch(PDOException $e)
       {
           echo "Close error:" . $e->getMessage() . PHP_EOL;
       }
   }


}
