<footer>
    <a href="#top">
        <img src="images/icon-back-to-top.png">
    </a>
    <p>
        &copy; <?php echo date("Y"); ?> Infinity Bits
     </p>
</footer>