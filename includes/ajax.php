<?php

    require_once('database.php'); 
    $SystemID = filter_input(INPUT_GET, 'SystemID', FILTER_VALIDATE_INT);
    
    if (!isset($SystemID))
    {
        $SystemID = filter_input(INPUT_GET, 'SystemID', FILTER_VALIDATE_INT);
        if ($SystemID == NULL || $SystemID == FALSE)
        {
            $SystemID = 1;
        }
    } 
    

$query1 = "SELECT systemName 
                  FROM system 
                  WHERE SystemID = :system_id";
$statement1 = $db->prepare($query1);
$statement1 ->bindValue(':system_id', $SystemID);
$statement1->execute();
$system = $statement1->fetch();
$system_name = $system['systemName'];
$statement1->closeCursor();
    
$query = 'SELECT game.GameID, gametitle.image, system.SystemName, gametitle.Developer, gametitle.GameName, gametitle.YearOfRelease, gametitle.Price
          FROM  game, system, gametitle 
          WHERE gametitle.GameID = game.GameID
          and system.SystemID = game.SystemID
          and system.SystemID = :system_id';
$statement = $db->prepare($query);
$statement->bindValue(':system_id', $SystemID);
$statement->execute();
$games = $statement->fetchAll();
$statement ->closeCursor();

$output = array();
$i = 0;

foreach($games as $game)
{
    $output[$i] = $game;
    $i++;
}

echo json_encode($output);